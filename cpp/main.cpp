#include "header/pcap_test.h"

int main(int argc, char* argv[]){
    if (argc != 2) {
        usage(argv);
        return -1;
    }
    char* dev = argv[1];
    char errbuf[PCAP_ERRBUF_SIZE]; //errbuf[256]
    pcap_t* handle = pcap_open_live(dev, BUFSIZ, 1, 1000, errbuf);
    if (handle == NULL) {
        fprintf(stderr, "couldn't open device %s: %s\n", dev, errbuf);
        return -1;
    }
    while (true) {
        struct pcap_pkthdr* header;
        const u_char* packet;
        int res = pcap_next_ex(handle, &header, &packet);

        if (res == 0) continue;
        if (res == -1 || res == -2) break;
        if (cmp_pkt(packet) == 1){ //Check IP & TCP packet
            struct libnet_ethernet_hdr* eth_hdr = (libnet_ethernet_hdr*)packet;
            struct libnet_ipv4_hdr* ipv4_hdr = (libnet_ipv4_hdr*)(packet+ETHER_LEN);
            int ip_len = (ipv4_hdr->ip_vhl & 0x0f) * 4;
            struct libnet_tcp_hdr* tcp_hdr = (libnet_tcp_hdr*)(packet+ETHER_LEN+ip_len);
            int total_len = ntohs(ipv4_hdr->ip_len), tcp_len = tcp_hdr->th_offx2 >> 2;
            size_t size = size_t(total_len - ip_len - tcp_len);
            uint16_t sport = ntohs(tcp_hdr->th_sport);
            uint16_t dport = ntohs(tcp_hdr->th_dport);

            print_mac(eth_hdr);
            print_ip(ipv4_hdr);
            print_port(tcp_hdr);
            if(sport == TCPPORT_HTTP || sport == TCPPORT_HTTPS || dport == TCPPORT_HTTP || dport == TCPPORT_HTTPS){ //Port 443(HTTPS), 80(HTTP)
                print_tcpdata(&packet[ETHER_LEN+ip_len+tcp_len], size);
            }
            printf("=======================================================================================================================================\n");
        }
    }
    pcap_close(handle);
    return 0;
}
