#include "header/pcap_test.h"

void usage(char **argv) {
  printf("Usage : %s <interface>\n",argv[0]);
  printf("sample : %s wlan0\n",argv[0]);
}

int cmp_pkt(const u_char *packet){ //If(packet == IP & TCP){ True }
    struct libnet_ethernet_hdr ether_hdr; struct libnet_ipv4_hdr ip_hdr;
    memcpy(&ether_hdr, packet, sizeof(ether_hdr));
    uint16_t eth_type = ntohs(ether_hdr.ether_type);
    if(eth_type == ETHERTYPE_IPV4 || eth_type == ETHERTYPE_IPV6){ //IPv4 0x0800, IPv6 0x08DD
        memcpy(&ip_hdr, packet+ETHER_LEN, sizeof(ip_hdr));
        if(ip_hdr.ip_p == IPPROTO_TCP){ //TCP 0x06
            return 1;
        }
    }
    return 0;
}

void print_mac(const libnet_ethernet_hdr *eth_hdr){ //Print Source MAC, Destination MAC
    const u_char* smac = eth_hdr->ether_shost;
    const u_char* dmac = eth_hdr->ether_dhost;
    printf("%02X:%02X:%02X:%02X:%02X:%02X -> ",smac[0],smac[1],smac[2],smac[3],smac[4],smac[5]);
    printf("%02X:%02X:%02X:%02X:%02X:%02X ",dmac[0],dmac[1],dmac[2],dmac[3],dmac[4],dmac[5]);
}

void print_ip(const libnet_ipv4_hdr *ipv4_hdr){ //Print Source IP, Destination IP
    u_char sip[4], dip[4];
    memcpy(&sip, &ipv4_hdr->ip_src.s_addr, sizeof(sip));
    memcpy(&dip, &ipv4_hdr->ip_dst.s_addr, sizeof(dip));
    printf("%d.%d.%d.%d -> ",sip[0],sip[1],sip[2],sip[3]);
    printf("%d.%d.%d.%d ",dip[0],dip[1],dip[2],dip[3]);
}


void print_port(const libnet_tcp_hdr *tcp_hdr){ //Print Source PORT, Destination PORT
    uint16_t sport = ntohs(tcp_hdr->th_sport);
    uint16_t dport = ntohs(tcp_hdr->th_dport);
    printf("%d -> %d ",sport, dport);
}

void print_tcpdata(const u_char *payload, size_t size){ //If(paylaod > 0){ Print TCP DATA }
    if(size>0){
        for(int i=0;i<MIN(size,16.0);i++){
            printf("%02X|", *payload+i);
        }
        printf("\n");
    }else{
        printf(" - ");
        printf("\n");
    }
}
